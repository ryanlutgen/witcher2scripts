# Witcher2Scripts
My modded base_scripts for The Witcher 2

Presently I have active:
- Give two talents per level instead of one
- Geralt can use his medallion again after one second instead of ten
- Always win dice poker
- Maximum bet of dice poker increased to 5,000 Orens
- Allow Geralt to use Dark items when on any difficulty.  This assumes that you have your own means to get the items, such as the mod that adds all items to innkeepers
- Remove camera effect when Dark weapons are drawn
